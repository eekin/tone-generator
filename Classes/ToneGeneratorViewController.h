//
//  ToneGeneratorViewController.h
//  ToneGenerator
//
//  Created by Erk Ekin on 2014/03/12.
//  Copyright 2010 Matt Gallagher. All rights reserved.
//
//  Permission is given to use this source code file, free of charge, in any
//  project, commercial or otherwise, entirely at your risk, with the condition
//  that any redistribution (in part or whole) of source code must retain
//  this copyright and permission notice. Attribution in compiled projects is
//  appreciated but not required.
//

#import <UIKit/UIKit.h>
#import <AudioUnit/AudioUnit.h>

@interface ToneGeneratorViewController : UIViewController
{
	AudioComponentInstance toneUnit;

@public
	double frequency;
	double sampleRate;
	double theta;
}

@property (strong, nonatomic) IBOutlet UISlider *frequencySlider;


- (IBAction)sliderChanged:(UISlider *)frequencySlider;

- (void)stop;

@end

