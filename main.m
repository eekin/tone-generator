    //
    //  main.m
    //  Anadolu

    //  Created by Erk EKİN on 4/21/13.
    //  Copyright (c) 2013 BAUM. All rights reserved.
    //

#import <UIKit/UIKit.h>

#import "ToneGeneratorAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ToneGeneratorAppDelegate class]));
    }
    
}
